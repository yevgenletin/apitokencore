﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPIAutores.DTOs;
using WebAPIAutores.Servicios;

namespace WebAPIAutores.Controllers
{

    [ApiController]
    [Route("api/llavesapi")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class LlavesAPIController: CustomBaseController
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly ServicioLLaves servicioLLaves;

        public LlavesAPIController(ApplicationDbContext context, IMapper mapper, ServicioLLaves servicioLLaves)
        {
            this.context = context;
            this.mapper = mapper;
            this.servicioLLaves = servicioLLaves;
        }

        [HttpGet]
        public async Task<List<LlaveDTO>> MisLlaves()
        {
            var usuarioId = ObtenerUsuario();
            var llaves = await context.LlaveAPI.Where(x=> x.UsuarioId == usuarioId).ToListAsync();

            return mapper.Map<List<LlaveDTO>>(llaves);
        
        }

        [HttpPost]
        public async Task<ActionResult> CrearLlave(CrearLlaveDTO crearLlaveDTO)
        {
            var usuarioId = ObtenerUsuario();

            if (crearLlaveDTO.TipoLlave == Entidades.TipoLlave.Gratuita)
            {
                var elUsuarioYaTieneUnaLlaveGratuita = await context.LlaveAPI
                    .AnyAsync(x => x.UsuarioId == usuarioId && x.TipoLlave == Entidades.TipoLlave.Gratuita);
                if (elUsuarioYaTieneUnaLlaveGratuita)
                {
                    return BadRequest("El usuario ya tiene una llave gratuita");
                }
            }
            await servicioLLaves.CrearLlave(usuarioId, crearLlaveDTO.TipoLlave);
            return NoContent();
            
        }

        [HttpPut]
        public async Task<ActionResult> ActualizarLlave(ActualizarLlaveDTO actualizarLlaveDTO) 
        {
            var usuarioId = ObtenerUsuario();
            var llaveDB = await context.LlaveAPI.FirstOrDefaultAsync(x=>x.Id == actualizarLlaveDTO.LlaveId);

            if (llaveDB == null)
            {
                return NotFound();
            }
            if (usuarioId != llaveDB.UsuarioId)
            {
                return Forbid();
            }

            if (actualizarLlaveDTO.ActualizarLlave)
            {
                llaveDB.Llave = servicioLLaves.GenerarLlave();

            }

            llaveDB.Activa = actualizarLlaveDTO.Activa;
            await context.SaveChangesAsync();
            return NoContent();
        }

    }
}
