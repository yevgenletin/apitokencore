﻿using System;
using System.Threading.Tasks;
using WebAPIAutores.Entidades;

namespace WebAPIAutores.Servicios
{
    public class ServicioLLaves
    {
        private readonly ApplicationDbContext context;

        public ServicioLLaves(ApplicationDbContext context)
        {
            this.context = context;
        }

        public async Task CrearLlave(string usuarioId, TipoLlave tipoLlave)
        {
            //permite generar un string aleatorio
            var llave = GenerarLlave();

            var llaveAPI = new LlaveAPI
            {
                Activa = true,
                Llave = llave,
                TipoLlave = tipoLlave,
                UsuarioId = usuarioId
            };

            context.Add(llaveAPI);

            await context.SaveChangesAsync();

        }

        public string GenerarLlave() 
        {
            return Guid.NewGuid().ToString().Replace("-","");
        }
    }
}
