﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Runtime.CompilerServices;
using WebAPIAutores.DTOs;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebAPIAutores.Entidades;
using System;
using System.Linq;
using System.Collections.Generic;
using WebAPIAutores.Migrations;

namespace WebAPIAutores.Middlewares
{
    public static class LimitarPeticionesMiddlewareExtensions
    {
        public static IApplicationBuilder UseLimitarPeticiones(this IApplicationBuilder app)
        {
            return app.UseMiddleware<LimitarPeticionesMiddleware>();
        }
    }

    public class LimitarPeticionesMiddleware
    {
        private readonly RequestDelegate request;
        private readonly IConfiguration configuration;

        public LimitarPeticionesMiddleware(RequestDelegate request, IConfiguration configuration)
        {
            this.request = request;
            this.configuration = configuration;
        }

        public async Task InvokeAsync(HttpContext httpContext, ApplicationDbContext context)
        {
            var limitarPeticionesConfiguracion = new LimitarPeticionesConfiguracion();
            configuration.GetRequiredSection("limitarPeticiones").Bind(limitarPeticionesConfiguracion);

            var ruta = httpContext.Request.Path.ToString();
            var estaListaEnRutaBlanca = limitarPeticionesConfiguracion.listaBlancaRutas.Any(x=>ruta.Contains(x));

            if (estaListaEnRutaBlanca) 
            {
                await request(httpContext);
                return;
            }

            //Estoy intentando obtener el valor de la llave.
            var llaveStringValues = httpContext.Request.Headers["X-Api-Key"];

            if (llaveStringValues.Count == 0)
            {
                httpContext.Response.StatusCode = 400;
                await httpContext.Response.WriteAsync("Debe proveer la llave de X-API-KEY");
                return;
            }

            if (llaveStringValues.Count > 1 )
            {
                httpContext.Response.StatusCode = 400;
                await httpContext.Response.WriteAsync("Solo una llave debe de estar presente");
                return;
            }

            var llave = llaveStringValues[0];
            var llaveDB = await context.LlaveAPI.FirstOrDefaultAsync(x=> x.Llave == llave);

            if (llaveDB == null)
            {
                httpContext.Response.StatusCode = 400;
                await httpContext.Response.WriteAsync("La llave introducida no es correcta.");
                return;

            }
            if (!llaveDB.Activa)
            {
                httpContext.Response.StatusCode = 400;
                await httpContext.Response.WriteAsync("La llave introducida no esta activa.");
                return;

            }

            if (llaveDB.TipoLlave == TipoLlave.Gratuita)
            {
                var hoy = DateTime.Today;
                var mañana = hoy.AddDays(1);

                var cantidadPeticionesRealizadasHoy = await context.Peticiones.CountAsync(x =>
                                                                                                x.LlaveId == llaveDB.Id &&
                                                                                                x.FechaPeticion >= hoy &&
                                                                                                x.FechaPeticion< mañana);
                if (cantidadPeticionesRealizadasHoy >= limitarPeticionesConfiguracion.PeticionesPorDiaGratuito)
                {
                    httpContext.Response.StatusCode = 429;
                    await httpContext.Response.WriteAsync("La llave ha accedido de las peticiones diarias permitidas por dia. Si desea puede contratar suscripcion premium.");
                    return;

                }

            }
            var superaRestricciones = PeticionSuperaAlgunaDeLasRestricciones(llaveDB,httpContext);
            if (!superaRestricciones)
            {
                httpContext.Response.StatusCode = 403;
                return;
            }
            var peticion = new Peticion()
            {
                LlaveId = llaveDB.Id,
                FechaPeticion = DateTime.UtcNow
            };
            context.Add(peticion);
            await context.SaveChangesAsync();


            await request(httpContext);
        }

        private bool PeticionSuperaAlgunaDeLasRestricciones(LlaveAPI llaveAPI, HttpContext httpContext)
        {
            var hayRestricciones = llaveAPI.RestriccionesDominios.Any() || llaveAPI.RestriccionesIP.Any();

            if (!hayRestricciones)
            {
                return true;
            }

            var peticionSuperaRestriccionesDominio = PeticionSuperaLasRestriccionesDeDominio(llaveAPI.RestriccionesDominios, httpContext);

            return peticionSuperaRestriccionesDominio;


        }

        private bool PeticionSuperaLasRestriccionesDeDominio(List<RestriccionDominio> restricciones, HttpContext httpContext)
        {
            if (restricciones == null || restricciones.Count == 0)
            {
                return false;

            }

            var referer = httpContext.Request.Headers["Referer"].ToString();

            if (referer == string.Empty)
            {
                return false;
            }

            Uri myUri = new Uri(referer);
            string host = myUri.Host;

            var superaRestriccion = restricciones.Any(x => x.Dominio == host);

            return superaRestriccion;

        }
    }

   



}
